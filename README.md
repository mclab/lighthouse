# Lighthouse

Lighthouse is a didactic tool written in Java that was developed as a student project. It allows to:

- define planning problems in the _lh_ format,
- generate PDDL files to feed to an external planner,
- generate SAT instances with two metodologies based on SATPLAN,
- execute Lua scripts and planning libraries to simplify problem encoding,
- generate an XML report that allows to discover the origin of each clause,
- visualize the generated report in a GUI.

# Documentation
See _doc/relazione.pdf_ for documentation, including implementation details, usage example and input format.

# Running it
Run with:
`java -jar dist/LightHouse.jar`.

## Example (_buckets_)
`java -jar dist/LightHouse.jar --satplan --pddl
--pddl-prefix buckets --domain examples/buckets.lhdomain --instance examples/buckets.lhinstance --show --dimacs-prefix sat
--maxt 9`

# People

Author: **Artem Ageev**

Contact person: **Marco Esposito** (<esposito@di.uniroma1.it>)



